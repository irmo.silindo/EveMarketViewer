//
//  ApiService.swift
//  MarketViewer
//
//  Created by CHETVERTKOV Kirill on 20.11.2021.
//

import Foundation

final class ApiService {
    static let shared = ApiService()
    
    struct Constants {
        static let topHeadLinesUrl = URL(string: "https://evemarketer.com/api/v1/markets/groups")
        static let searchSuggestionsUrl = URL(string: "http://evemarketer.com/api/v1/types/autocomplete?q=acol&language=en")
    }
    
    private init() {
        
    }
    
    public func getTopStories(completion: @escaping (Result<[APIResponse], Error>) -> Void) {
        guard let url = Constants.topHeadLinesUrl else { return }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completion(.failure(error))
            }
            
            else if let data = data {
                do {
                    let result:[APIResponse] = try JSONDecoder().decode([APIResponse].self, from: data)
                    print("Groups count: \(result.count)")
                    completion(.success(result))
                } catch {
                    completion(.failure(error))
                }
            }
        }
        task.resume()
    }
    
    public func search(with query: String, completion: @escaping (Result<[SearchResponse], Error>) -> Void) {
        guard !query.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {return}
        guard let url = Constants.searchSuggestionsUrl else { return }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completion(.failure(error))
            }
            
            else if let data = data {
                do {
                    let result:[SearchResponse] = try JSONDecoder().decode([SearchResponse].self, from: data)
                    print("Suggestions count: \(result.count)")
                    completion(.success(result))
                } catch {
                    completion(.failure(error))
                }
            }
        }
        task.resume()
    }
    
    // Models
    struct APIResponse: Codable {
        let id: Int
        let parent_id: Int
        let name: String?
        let description: String?
        let icon_id: Int
        let has_types: Int
        let childrens: [APIResponse]?
    }
    
    struct SearchResponse: Codable {
        let id: Int
        let description: String?
        let name: String
    }

}

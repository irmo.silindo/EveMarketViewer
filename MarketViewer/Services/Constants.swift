//
//  Constants.swift
//  MarketViewer
//
//  Created by CHETVERTKOV Kirill on 21.11.2021.
//

import Foundation
import UIKit

extension NSObject {
    class Constants {
        static var backgroundColor = UIColor.darkGray
        static var transparentBackgroundColor = UIColor.clear
        static var textPrimaryColor = UIColor.systemGray3
        static var textSecondaryColor = UIColor.systemGray2
    }
}

//
//  SearchTableViewCell.swift
//  MarketViewer
//
//  Created by CHETVERTKOV Kirill on 21.11.2021.
//

import UIKit

class searchTableViewCellViewModel {
    let id: Int
    let description: String
    let name: String
    var imageData: Data? = nil
    
    init(
        id: Int,
        description: String,
        name: String
    ) {
        self.id = id
        self.description = description
        self.name = name
    }
}

class SearchTableViewCell: UITableViewCell {
    static let identifier = "SearchTableViewCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with viewModel: searchTableViewCellViewModel) {
        self.backgroundColor = Constants.backgroundColor
        lbl.text = viewModel.name
        
        //Image
        if let data = viewModel.imageData {
            img.image = UIImage(data: data)
        } else {
            //fetch
            if let url = URL(string: "https://imageserver.eveonline.com/Type/\(viewModel.id)_64.png") {
                URLSession.shared.dataTask(with: url) { data, _, error in
                    guard let data = data, error == nil else { return }
                    viewModel.imageData = data
                    DispatchQueue.main.async {
                        self.img.image = UIImage(data: data)
                    }
                }.resume()
            }
        }
    }
    
}

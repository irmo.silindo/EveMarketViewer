//
//  ProductTableViewCell.swift
//  MarketViewer
//
//  Created by CHETVERTKOV Kirill on 22.11.2021.
//

import UIKit

class ProductTableViewCellViewModel{
    let id: Int
    let quantity: Int
    let price: Decimal
    let securityLevel: Float
    
    
    init(
        id: Int,
        quantity: Int,
        price: Decimal,
        securityLevel: Float
    ) {
        self.id = id
        self.quantity = quantity
        self.price = price
        self.securityLevel = securityLevel
    }
}

class ProductTableViewCell: UITableViewCell {
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    

    static let identifier = "ProductTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with viewModel: ProductTableViewCellViewModel) {
        self.backgroundColor = Constants.backgroundColor
        quantityLbl.text = "Qty:"+String(viewModel.quantity)
        priceLbl.text = "\(viewModel.price)"+"ISK"
        statusLbl.text = "\(viewModel.securityLevel)"
    
    }
    
}

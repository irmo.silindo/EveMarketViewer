//
//  ViewController.swift
//  MarketViewer
//
//  Created by CHETVERTKOV Kirill on 20.11.2021.
//

import UIKit

//TableView
//CustomCell
//CallerApi
//open elem
//searching elem


class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    private let tableView: UITableView = {
       let table = UITableView()
        table.register(HomeTableViewCell.self, forCellReuseIdentifier: HomeTableViewCell.identifier)
        table.backgroundColor = Constants.backgroundColor
        
        return table
    }()
    
    private var viewModels = [HomeTableViewCellViewModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Browse items"
        self.tableView.tintColor = Constants.textPrimaryColor
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        view.backgroundColor = Constants.backgroundColor
        configureItems()
        
        ApiService.shared.getTopStories {[weak self] result in
            switch (result) {
                case .success(let response):
                self?.viewModels = response.compactMap({
                    HomeTableViewCellViewModel(
                        id: $0.id,
                        parentId: $0.parent_id,
                        title: $0.name ?? "no title",
                        description: $0.description ?? "no description",
                        iconId: $0.icon_id,
                        hasTypes: $0.has_types)
                })
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
             //   break
            case .failure(let error): print(error)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }

    //Table
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: IndexPath)
//        cell.textLabel?.text = "Something"
//        return cell
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.identifier, for: indexPath) as? HomeTableViewCell else {
            fatalError()
        }
        cell.configure(with: viewModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let viewModel = viewModels[indexPath.row]
        didselectProduct(viewModel: viewModel)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    
    private func configureItems() {
        self.navigationItem.rightBarButtonItems = [
            UIBarButtonItem(
            barButtonSystemItem: .search,
            target: self,
            action: #selector(didTapSearchButton)
            ),
            
            UIBarButtonItem(
                image: UIImage(systemName: "person.circle"),
                style: .done,
                target: self,
                action: #selector(didTapProfileButton)
            )
        ]
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "questionmark.app"),
            style: .done,
            target: self,
            action: #selector(didTapSettingsButton)
        )
    }
    
    @objc func didTapSearchButton() {
        let vc = SearchViewController()
        
        vc.title = "Search an item"
        vc.view.backgroundColor = Constants.backgroundColor
        vc.navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "books.vertical"),
            style: .plain,
            target: self,
            action: nil
        )

        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTapSettingsButton() {
        let vc = AboutViewController()
    
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTapProfileButton() {
        let vc = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func didselectProduct(viewModel: HomeTableViewCellViewModel) {
        let vc = ProductViewController(nibName: "ProductViewController", bundle: nil)
        vc.setSelectedProductId(id: viewModel.id)
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
}


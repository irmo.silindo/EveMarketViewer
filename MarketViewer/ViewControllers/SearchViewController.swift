//
//  SearchViewController.swift
//  MarketViewer
//
//  Created by CHETVERTKOV Kirill on 21.11.2021.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate {
    
    let searchVC = UISearchController(searchResultsController: nil)
    private var viewModels = [searchTableViewCellViewModel]()
    
    private let searchTableView: UITableView = {
       let table = UITableView()
        table.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: SearchTableViewCell.identifier)
        table.backgroundColor = Constants.backgroundColor
        
        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.searchController = searchVC
        searchVC.searchBar.delegate = self
        
        view.addSubview(searchTableView)
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchTableView.frame = CGRect(x:40, y: 70, width: view.frame.size.width - 70, height: view.frame.size.height - 70)
    }
    
    //Search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, !text.isEmpty else {return}
        
        print(text)
       // ApiService.shared.search(with: text, completion: <#T##(Result<[ApiService.SearchResponse], Error>) -> Void#>)
        
        
        ApiService.shared.search(with: "acolyte") {[weak self] result in
            switch (result) {
                case .success(let response):
                self?.viewModels = response.compactMap({
                    searchTableViewCellViewModel(
                        id: $0.id,
                        description: $0.description ?? "no description",
                        name: $0.name
                        )
                })
                DispatchQueue.main.async {
                    self?.searchTableView.reloadData()
                }
             //   break
            case .failure(let error): print(error)
            }
        }
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = searchTableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.identifier, for: indexPath) as? SearchTableViewCell {
            cell.configure(with: viewModels[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
}

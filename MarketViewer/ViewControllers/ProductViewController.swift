//
//  ProductViewController.swift
//  MarketViewer
//
//  Created by CHETVERTKOV Kirill on 22.11.2021.
//

import UIKit

class ProductViewController: UIViewController {

    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productLabel: UILabel!
    
    @IBOutlet weak var listTable: UITableView!
    
    private var productId : Int? = nil
    private var viewModels = [ProductTableViewCellViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.listTable.delegate = self
        self.listTable.dataSource = self
        view.backgroundColor = Constants.backgroundColor
        listTable.backgroundColor = Constants.backgroundColor
        listTable.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: ProductTableViewCell.identifier)
        
        viewModels.append(ProductTableViewCellViewModel(
            id: 123, quantity: 44, price: 3392092911, securityLevel: 0.65
        ))
        listTable.reloadData()

        // Do any additional setup after loading the view.
    }
    
    func setSelectedProductId(id: Int) {
        self.productId = id
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension ProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = listTable.dequeueReusableCell(withIdentifier: ProductTableViewCell.identifier, for: indexPath) as? ProductTableViewCell {
            cell.configure(with: viewModels[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
}

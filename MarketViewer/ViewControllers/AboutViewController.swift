//
//  SettingsViewController.swift
//  MarketViewer
//
//  Created by CHETVERTKOV Kirill on 21.11.2021.
//

import UIKit

class AboutViewController: UIViewController, UIScrollViewDelegate {
    static var aboutText: String = """
    "CCP Copyright Notice
    EVE Online and the EVE logo are the registered trademarks of CCP hf. All rights are reserved worldwide. All other trademarks are the property of their respective owners. EVE Online, the EVE logo, EVE and all associated logos and designs are the intellectual property of CCP hf. All artwork, screenshots, characters, vehicles, storylines, world facts or other recognizable features of the intellectual property relating to these trademarks are likewise the intellectual property of CCP hf. CCP hf. has granted permission to EVEMarketer to use EVE Online and all associated logos and designs for promotional and information purposes on its website but does not endorse, and is not in any way affiliated with, EVEMarketer. CCP is in no way responsible for the content on or functioning of this website, nor can it be liable for any damage arising from the use of this website.
"""
    
    private let aboutLabel: UILabel = {
       let label = UILabel()
        label.font = .systemFont(ofSize: 25, weight: .medium)
        label.backgroundColor = Constants.transparentBackgroundColor
        label.textColor = Constants.textPrimaryColor
        label.numberOfLines = 0
        label.text = aboutText
        return label
    }()
    
    let tv: UITextView = {
        let textView = UITextView(frame: CGRect(x: 20.0, y: 20.0, width: 20.0, height: 20.0))
    
    textView.contentInsetAdjustmentBehavior = .automatic
     
     //textView.center = self.view.center
     textView.textAlignment = NSTextAlignment.justified
        textView.backgroundColor = Constants.transparentBackgroundColor
    
     
     // Update UITextView font size and colour
     textView.textColor = UIColor.white
     textView.font = UIFont(name: "Verdana", size: 17)
     
     // Capitalize all characters user types
     textView.autocapitalizationType = UITextAutocapitalizationType.allCharacters
     
     // Make UITextView web links clickable
     textView.isSelectable = false
     textView.isEditable = false
     textView.isScrollEnabled = false
     textView.dataDetectorTypes = UIDataDetectorTypes.link
     
     // Make UITextView corners rounded
     textView.layer.cornerRadius = 10
     
     // Enable auto-correction and Spellcheck
     textView.autocorrectionType = UITextAutocorrectionType.yes
     textView.spellCheckingType = UITextSpellCheckingType.no
     // myTextView.autocapitalizationType = UITextAutocapitalizationType.None
     
     // Make UITextView Editable
        textView.text = aboutText
       // textView.sizeToFit()
    return textView
    }()
    
//    lazy var scrollView: UIScrollView = {
//        let scroll = UIScrollView()
//        scroll.translatesAutoresizingMaskIntoConstraints = false
//        scroll.delegate = self
//        scroll.contentSize = CGSize(width: self.view.frame.size.width, height: tv.frame.height)
//        return scroll
//    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareIt()

        view.addSubview(tv)

        // Do any additional setup after loading the view.
    }
    
    
    private func prepareIt() {
        self.title = "About"
        self.view.backgroundColor = .Constants.backgroundColor
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tv.frame = CGRect(x: 20.0, y: 20.0, width: self.view.frame.size.width - 40, height: self.view.frame.size.height - 10)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

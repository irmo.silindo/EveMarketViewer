//
//  NewsTableViewCell.swift
//  MarketViewer
//
//  Created by CHETVERTKOV Kirill on 20.11.2021.
//

import UIKit

class HomeTableViewCellViewModel {
    let id: Int
    let parentId: Int
    let title: String
    let description: String
    let iconId: Int
    let hasTypes: Int
    var imageData: Data? = nil
    
    init(
        id: Int,
        parentId: Int,
        title: String,
        description: String,
        iconId: Int,
        hasTypes: Int
    ) {
        self.id = id
        self.parentId = parentId
        self.title = title
        self.description = description
        self.iconId = iconId
        self.hasTypes = hasTypes
    }
}

class HomeTableViewCell: UITableViewCell {
    static let identifier = "HomeTableViewCell"
    
    private let homeTitleLabel: UILabel = {
       let label = UILabel()
        label.font = .systemFont(ofSize: 25, weight: .medium)
        label.backgroundColor = Constants.transparentBackgroundColor
        label.textColor = Constants.textPrimaryColor
        label.numberOfLines = 0
        return label
    }()
    
    private let subtitleLabel: UILabel = {
       let label = UILabel()
        label.font = .systemFont(ofSize: 18, weight: .regular)
        label.backgroundColor = Constants.transparentBackgroundColor
        label.textColor = Constants.textSecondaryColor
        label.numberOfLines = 0
        return label
    }()
    
    private let homeImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.backgroundColor = Constants.transparentBackgroundColor
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 8
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(homeImageView)
        contentView.addSubview(homeTitleLabel)
        contentView.addSubview(subtitleLabel)
            }
   
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        homeTitleLabel.frame = CGRect(x: 10, y: 0, width: contentView.frame.size.width - 170, height: 70)
        
        subtitleLabel.frame = CGRect(x: 10, y: 70, width: contentView.frame.size.width - 170, height: 70)
        
        homeImageView.frame = CGRect(x: contentView.frame.size.width-150, y: 5, width: 160, height: contentView.frame.size.height - 10)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        homeTitleLabel.text = nil
        subtitleLabel.text = nil
        homeImageView.image = nil
    }
    
    func configure(with viewModel: HomeTableViewCellViewModel) {
        self.backgroundColor = Constants.backgroundColor
        homeTitleLabel.text = viewModel.title
        subtitleLabel.text = viewModel.description
        
        //Image
        if let data = viewModel.imageData {
            homeImageView.image = UIImage(data: data)
        } else {
            //fetch
            if let url = URL(string: "https://evemarketer.com/static/img/market_groups/\(viewModel.iconId).png") {
                URLSession.shared.dataTask(with: url) { data, _, error in
                    guard let data = data, error == nil else { return }
                    viewModel.imageData = data
                    DispatchQueue.main.async {
                        self.homeImageView.image = UIImage(data: data)
                    }
                }.resume()
            }
        }
    }
}
